using client;
public class Compte
{
    
    public float Solde { get; private set; }
    public uint NumCompte { get; private set; }
    public Client Proprio { get; set; }
    

    private static uint s_compteur = 0; 

    
    public Compte()
    {
        Solde = default; 
        NumCompte = ++s_compteur; 
        Proprio = new(); 
    }

    
    public Compte(Client proprio) :
        this() 
    {
        Proprio = proprio ?? new();

    }
    public Compte(float solde, Client proprio) :
        this(proprio)
    {
        Solde = solde; 
    }

    
    public void Crediter(float montant)
    {
        Solde += montant; 
    }

    
    public void Crediter(float montant, Compte crediteur)
    {
        Crediter(montant); 
        crediteur.Debiter(montant); 
    }

    // Méthode pour débiter le compte
    public void Debiter(float montant)
    {
        Solde -= montant; 
    }

    
    public void Debiter(float montant, Compte debiteur)
    {
        Debiter(montant); 
        debiteur.Crediter(montant); 
    }

    
    public string Afficher()
    {
     
      return $@"
        
        ___________________________________
        |Numéro de Compte: {NumCompte}
        |Solde de compte: {Solde}
        |Propriétaire du compte :
        |{Proprio.Afficher()}
        |__________________________________";
    }

    
    public static string NbCompte()
    {
        return $"Le nombre de comptes créés: {s_compteur}";
    }
    
}
